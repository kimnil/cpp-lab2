#ifndef JULIAN_H
#define JULIAN_H
#include "date.h"
#include <string>

// Namespace enligt labbpek.
namespace lab2 {

    class Julian : public Date {


        public:
        // Number of days in each month
        static const int DAYS_PER_MONTH[12];
        static const int MONTHS_PER_YEAR;
        static const std::string MONTH_NAMES[12];
        static const std::string DAY_NAMES[7];

        enum DAYS {
                MONDAY = 1, 
                TUESDAY = 2,
                WEDNESDAY = 3,
                THURSDAY = 4,
                FRIDAY = 5,
                SATURDAY = 6,
                SUNDAY = 7
            };

        enum MONTHS {
                JANUARY = 1, 
                FEBRUARY = 2,
                MARCH = 3,
                APRIL = 4,
                MAY = 5,
                JUNE = 6,
                JULY = 7,
                AUGUST = 8,
                SEPTEMBER = 9,
                OCTOBER = 10,
                NOVEMBER = 11,
                DECEMBER = 12
            };

        // We dont manage any dymanic memory
        // so this constructor will be simple
        Julian(void);
        Julian(int year, int month, int day);

        // Note: Keep destructor virtual! Otherwise derived
        // classes might call the base class implementation
        // of the destructor and leave garbage in memory.
        virtual ~Julian(void);

        // Assignment operator
        // No reason to have this virtual
        // Wont have the same signature in sub-classes
        Julian& operator=(const Julian& rhs);
        using Date::operator=; // This will be shadowed unless we do this.

        // Copy constructor
        Julian(const Julian & j);
        Julian(const Date & rhs);

        // Conversion constructor
        Julian(Date const * const dp);

        // postﬁx-operatorer ++ och -- som returnerar kopia av sig själv. 
        Julian operator++(int);
        Julian operator--(int);


        virtual int days_this_month(void) const; // This is dependent on both calendar and current month
        virtual std::string week_day_name(void) const;
        virtual std::string month_name(void) const;
        virtual int week_day(void) const {return (((mod_julian_day() + 2400001) % 7) + 1); }

        // Should be private but prevents testing.
        public:
        virtual int days_in_month(int month) const;
        virtual int mod_julian_day(void) const;
        virtual void set_date_from_mjd(const int mjd);
        virtual bool leap_year(int n = 0) const;
    };

}
#endif
