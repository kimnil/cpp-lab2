#ifndef CALENDAR_H
#define CALENDAR_H
#include <string>
#include <iostream>
#include <vector>
#include "kattistime.h"
#include "time.h"

// Namespace enligt labbpek.
namespace lab2 {

    template<class T>
    class Calendar {
        class Event {
            public:
            std::string description;
            T m_date;
        };

        //private:
        public:
        std::vector<Event> m_events;
        T m_current_date;

        public:
        Calendar<T>(void);

        template<class S>
        Calendar<T>(Calendar<S> c);

        // Destructor
        ~Calendar<T>(void);

        // Assignment operator
        template<class S>
        Calendar<T>& operator=(const Calendar<S>& rhs);

        Calendar<T>& operator=(const Calendar<T>& rhs);

        bool set_date(int year, int month, int day);
        bool add_event(std::string description, int day, int month, int year);
        bool add_event(std::string description, int day, int month);
        bool add_event(std::string description, int day);
        bool add_event(std::string description);
        bool remove_event(std::string description, int day, int month, int year);
        bool remove_event(std::string description, int day, int month);
        bool remove_event(std::string description, int day);
        bool remove_event(std::string description);

        int year() const { return m_current_date.year(); }
        int month() const { return m_current_date.month(); }
        int day() const { return m_current_date.day(); }

        template<class S> // lite oklart
        friend std::ostream& operator<<( std::ostream& os, const Calendar<T>& calendar );
    };
}
#include "calendar.tpp"
#endif
