#ifndef DATE_H
#define DATE_H
#include <string>
#include <iostream>
#include <stdio.h>

// Namespace enligt labbpek.
namespace lab2 {

    class Date {

        public:
        virtual int days_in_month(int month) const = 0;

        // We dont manage any dymanic memory
        // so this constructor will be simple
        Date(void);
        Date(int year, int month, int day);

        // Note: Keep destructor virtual! Otherwise derived
        // classes might call the base class implementation
        // of the destructor and leave garbage in memory.
        virtual ~Date(void);

        // Assignment operator
        // No reason to have this virtual
        // Wont have the same signature in sub-classes
        Date& operator=(Date const & rhs);

        // Copy constructor
        Date(const Date& rhs);

        int year(void) const { return m_year; }
        int month(void) const { return m_month; }
        int day(void) const { return m_day; }

        int days_per_week(void) const { return m_days_per_week; }
        int months_per_year(void) const { return m_months_per_year; }

        virtual int week_day(void) const = 0;
        virtual int days_this_month(void) const = 0; // This is dependent on both calendar and current month
        virtual std::string week_day_name(void) const = 0;
        virtual std::string month_name(void) const = 0;

        // Prefix operator ++ and -- 
        Date& operator++(void);
        Date& operator--(void);

        // Should return what? Since sub-classes of Date has the same interface. Date& will do fine
        Date& operator+=(int i); 
        Date& operator-=(int i);

        Date& add_year(int n = 1); // Should return what? Motivate! Reference to date, allows chaining?
        Date& add_month(int n = 1); // Note: default params are looked up statically.
        Date& add_day(int n = 1); // Note: default params are looked up statically.

        // Compare two dates
        bool operator==(Date const & rhs) const;
        bool operator!=(Date const & rhs) const;
        bool operator<(Date const & rhs) const;
        bool operator<=(Date const & rhs) const;
        bool operator>(Date const & rhs) const;
        bool operator>=(Date const & rhs) const;

        // Difference in days between two dates, fre - ons = 2
        int operator-(Date const & rhs) const;

        // Let the print function see our date subclasses inner workings!
        friend std::ostream& lab2::operator<<(std::ostream & os, Date const & date);
       
        // private: // Testing
        public:
        int m_year, m_month, m_day;
        int m_days_per_week, m_months_per_year;

        virtual int mod_julian_day(void) const = 0;
        virtual void set_date_from_mjd(int mjd) = 0;
        virtual bool leap_year(int n = 0) const = 0;

    };

    // Our date printer
    std::ostream& operator<<(std::ostream & os, Date const & date);

}
#endif
