#include "date.h"
#include <iostream>
#include <iomanip> // for output padding eg. (2010-01-02)
#include <cmath> // for abs()

#define DEBUG 0

// Skip lab2:: everywhere.
using namespace lab2;

/**
  * This is the default constructor.
  */
Date::Date(void) {
    if (DEBUG) {
        std::cout << std::endl << "lab2::Date(void) - default konstruktorn" << std::endl;
    } 
}

/**
  * This contstructor creates a Date with a given date.
  */
Date::Date(int year, int month, int day) {
    if (DEBUG) {
        std::cout << std::endl << "lab2::Date(int, int, int)" << std::endl;
    } 

    m_year = year;
    m_month = month;
    m_day = day;
}

/**
  * Copy constructor 
  */
Date::Date(const Date& rhs) {
    *this = rhs;
}

/**
  * Destruktor
  */
Date::~Date(void) {
    if (DEBUG) {
        std::cout << std::endl << "lab2::~Date(void) - default dekonstruktorn" << std::endl;
    } 
}

/** 
  * Assignment operator 
  */
Date& Date::operator=(Date const & rhs) { 
    int mjd = rhs.mod_julian_day();
    this->set_date_from_mjd(mjd);
    return *this;
}

/**
 * Compare the number of days since julian mod of two days
 * If the dates are in different calendars, the date doesnt have to match
 */
bool Date::operator==(Date const & rhs) const { 
    return this->mod_julian_day() == rhs.mod_julian_day();
}

bool Date::operator!=(Date const & rhs) const {
    return this->mod_julian_day() != rhs.mod_julian_day();
}

bool Date::operator<(Date const & rhs) const {
    return this->mod_julian_day() < rhs.mod_julian_day();
}

bool Date::operator<=(Date const & rhs) const {
    return this->mod_julian_day() <= rhs.mod_julian_day();
}

bool Date::operator>(Date const & rhs) const {
    return this->mod_julian_day() > rhs.mod_julian_day();
}

bool Date::operator>=(Date const & rhs) const {
    return this->mod_julian_day() >= rhs.mod_julian_day();
}

/**
  * Add i days 
  */
Date& Date::operator+=(int i) {
    this->add_day(i);
    return *this;
}

/**
  * Remove i days 
  */
Date& Date::operator-=(int i) {
    this->add_day(-i);
    return *this;
}

/**
 * Difference between two days
 * 2011-01-05 - 2011-01-04 = 1
 */
int Date::operator-(Date const & rhs) const {
    return this->mod_julian_day() - rhs.mod_julian_day();
}

/**
  * Increase day by one.
  */
Date& Date::operator++() {
    this->add_day(1);
    return *this;
}

/**
  * Increase day by one.
  */
Date& Date::operator--() {
    this->add_day(-1);
    return *this;
}

/**
  * Add a number of months.
  * Defaults to n = 1
  */
Date& Date::add_month(int n) {
    int current_mjd;
    // Are we adding or subtracting months?
    int direction = ( n > 0 ) ? 1 : -1;

    // Recursive base case - "as long as n is not zero"
    if (std::abs(n) != 0) {

        // does todays date exist in the month we are heading towards?
        if (m_day <= days_in_month( m_month + direction)) {
            // Add one month and make sure its between 1 - 12 
            // also, don't forget to add a new year if needed be.
            if (direction > 0) {
                if (++m_month > 12) {
                    m_month = 1;
                    m_year++;
                }
            } else {
                if (--m_month < 1) {
                    m_month = 12;
                    m_year--;
                }
            }
        } else {
            // add 30 days
            current_mjd = mod_julian_day();
            set_date_from_mjd(current_mjd + 30 * direction);
        }

        // Recursive call
        add_month( n - direction );
    }

    return *this;

    // m_day = 31, m_month = 3
    /*
    if (n > 0) {
        if (m_day > days_in_month( (m_month + 1) % months_per_year() )) {
            // add 30 days
            current_mjd = mod_julian_day();
            set_date_from_mjd(current_mjd + 30);
        } else {
            // add days in month
            current_mjd = mod_julian_day();
            set_date_from_mjd(current_mjd + days_this_month());
        }
        if (n > 1) {
            add_month(--n);
        }
        return *this;
    } else if (n < 0) {
        if (m_day > days_in_month( m_month - 2 )) {
            // subtract 30 days
            current_mjd = mod_julian_day();
            set_date_from_mjd(current_mjd - 30);
        } else {
            // subtract days in previous month
            current_mjd = mod_julian_day();
            set_date_from_mjd(current_mjd - days_in_month( m_month - 2 ));
        }
        if (n < -1)
            add_month(++n);
        return *this;

    }

    return *this;
    */
}

/**
  * Add a number of days.
  * Defaults to n = 1
  */
Date& Date::add_day(int n) {
    int current_mjd;
    if (DEBUG) {
        std::cout << std::endl << "add_day(" << n << ")" << std::endl;
    }

    // Calculate new date based off MJD.
    current_mjd = mod_julian_day();
    set_date_from_mjd(current_mjd + n);
    return *this;
}

/**
  * Add a number of years.
  * Defaults to n = 1
  */
Date& Date::add_year(int n) {
    if (n == 0)
        return *this;

    // Leap year and divisble by 4
    if (leap_year() && leap_year(n)) {
        m_year += n;
    } else {

        // Ugh for the leap day.
        if (leap_year() && m_month == 2 && m_day == 29) {
            m_day--;
        }

        //m_year += n;

        if (n >= 1) {
            m_year++;
            add_year(--n);
        }
        else if (n <= -1) {
            m_year--;
            add_year(++n);
        }
    }

    return *this;
}

/**
 * Prints the date on the format YYYY-MM-DD
 * returns the ostream for chaining
 */
std::ostream& lab2::operator<<(std::ostream & os, lab2::Date const & date) {
    using namespace std;

    // Pad with 0's
    os << setfill('0');

    os << setw(4) << date.year() << "-" <<  setw(2) << date.month() << "-" <<  setw(2) << date.day();
    os << std::resetiosflags(std::ios::showbase);

    return os;
}
