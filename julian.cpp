#include "julian.h"
#include "gregorian.h"
#include <iostream>
#include "kattistime.h"
#include <stdexcept>      // std::out_of_range

#define DEBUG 0

// Skip lab2:: everywhere.
using namespace lab2;

const int Julian::DAYS_PER_MONTH[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const int Julian::MONTHS_PER_YEAR = 12;
const std::string Julian::MONTH_NAMES[12] = {
                                    std::string("january"),
                                    std::string("february"),
                                    std::string("march"),
                                    std::string("april"),
                                    std::string("may"),
                                    std::string("june"),
                                    std::string("july"),
                                    std::string("august"),
                                    std::string("september"),
                                    std::string("october"),
                                    std::string("november"),
                                    std::string("december")
                                   };

const std::string Julian::DAY_NAMES[7] = {
                                    std::string("monday"),
                                    std::string("tuesday"),
                                    std::string("wednesday"),
                                    std::string("thursday"),
                                    std::string("friday"),
                                    std::string("saturday"),
                                    std::string("sunday")
                                   };

/**
  * This is the default constructor.
  */
Julian::Julian(void) 
    : Date() {

    int g_year, g_month, g_day;

    if (DEBUG) {
        std::cout << std::endl << "Julian::Julian(void) - default konstruktorn" << std::endl;
    } 

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    time_t mytime;
    k_time(&mytime);
    struct tm *t = gmtime(&mytime);

    g_year = t->tm_year + 1900;
    g_month = t->tm_mon + 1;
    g_day = t->tm_mday;

    Gregorian today_g(g_year, g_month, g_day);
    Julian today_j(1,2,3);
    today_j.set_date_from_mjd(today_g.mod_julian_day());

    m_year = today_j.year();
    m_month = today_j.month();
    m_day = today_j.day();
}

/**
  * Year, month, day constructor
  */
Julian::Julian(int year, int month, int day) 
    : Date(year, month, day) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    // Make sure it's a valid date!
    if( (day <= 0 || month <= 0 || year < 0) ||
        day > 31 || month > 12) {
        throw std::out_of_range("no such date!");
    }

    if (day > days_in_month(month)) {
        throw std::out_of_range("no such date!");
    }

    if (DEBUG) {
        std::cout << std::endl << "Julian::Julian(int, int, int)" << std::endl;
    } 
}

/**
  * Default copy constructor.
  */
Julian::Julian(const Julian & j) {
    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    *this = j;
}

/**
  * Construct from date.
  */
Julian::Julian(const Date & rhs) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    *this = rhs;
}

/**
  * Construct from date pointer (conversion constructor).
  */
Julian::Julian(Date const * const dp) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    *this = *dp;
}

/**
  * Deconstructor.
 */
Julian::~Julian(void) {
    if (DEBUG) {
        std::cout << std::endl << "Julian::~Julian(void) - default dekonstruktorn" << std::endl;
    } 
}

/**
  * Return the name of the current day, eg, "wednesday"
  */
std::string Julian::week_day_name(void) const {
    return DAY_NAMES[(mod_julian_day() + 2400001) % 7];
}

/**
  * Return the name of the current month, eg, "january"
  */
std::string Julian::month_name(void) const {
    return MONTH_NAMES[m_month - 1];
}

/**
  * Returns the number of day from this day to MJD.
  */
int Julian::mod_julian_day(void) const {
    int a, y, m, jd, res;

    a = ((14 - m_month) / 12);
    y = m_year + 4800 - a;
    m = m_month + 12*a - 3;

    jd = m_day + (153*m + 2) / 5  + 365*y + y/4 - 32083;

    res = jd - 2400000.5;
    // fulhack för att fixa MJD blir ett för mycket vid negativa tal, innan datumet nedan
    if (res < 0 || (m_year == 1858 && m_month <= 11 && m_day < 5)) {
        res--;
    }

    return res;
}

/**
  * Update the year, month and day of this object to
  * the corresponding MJD.
  */
void Julian::set_date_from_mjd(const int mjd) {
    int b, c, d, e, m;
    int D, M, Y;

    b = 0;
    c = mjd + 2400001 + 32082;

    d = (4 * c + 3) / 1461;
    e = c - (1461 * d) / 4;
    m = ( 5 * e + 2) / 153;

    D = e - ( 153 * m + 2) / 5 + 1;
    M = m + 3 - 12 * (m / 10);
    Y = b * 100 + d - 4800 + m / 10;

    m_year = Y;
    m_month = M;
    m_day = D;
}


/**
  * Override default Assignment operator.
  */
Julian& Julian::operator=(const Julian& rhs) {
    // Read the modified julian day offset from the rhs
    // and set the date of this corresponding to that offset.
    //int mjd = rhs.mod_julian_day();
    //this->set_date_from_mjd(mjd);
    Date::operator=(rhs);

    return (*this);
}

/**
  * Increase day by one.
  */
Julian Julian::operator++(int dummy) {
    Julian tmp(*this);
    this->add_day(1);
    return tmp;
}

/**
  * Increase day by one.
  */
Julian Julian::operator--(int dummy) {
    Julian tmp(*this);
    this->add_day(-1);
    return tmp;
}

/**
  * Returns true if current year is a leap year.
  */
bool Julian::leap_year(int n) const {
    // any year not dividble by 4
    return !((m_year + n) % 4);
}

/**
  * Returns the number of days in some month.
  * parameter month is 1-indexed so 1 = JANUARY
              also, input will be 12-modded so 15 would be 3 = march
 */
int Julian::days_in_month(int month) const {
   month = month - 1; // 0 index
   month = (month + months_per_year()) % months_per_year(); // make sure -1 is 11 etc;

    // From now on month is 0 indexed, hence FEBRUARY == 1
    if (leap_year() && month == 1) {
        return 29;
    } else {
        return DAYS_PER_MONTH[month]; // Note, add one
    }
}

/**
  * Returns the number of days in the current month.
 */
int Julian::days_this_month(void) const {
    return days_in_month(m_month);
}
