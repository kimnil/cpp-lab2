#ifndef TEST_H_K
#define TEST_H_K
#include "/info/cprog09/cxxtest/cxxtest/TestSuite.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include "date.h"
#include "gregorian.h"
#include "calendar.h"
#include "julian.h"
#include <stdio.h>
#include <stdexcept>      // std::out_of_range

using namespace lab2;
using namespace std;

class DateTestSuite : public CxxTest::TestSuite
{

    public:
    void testTemp(void) {
        {
            Julian j(2319, 1, 7);
            Gregorian g = j;
            Julian j2 = j;

            Date * d = &g; 
            *d = j;

            //std::cout << *d << endl;
        }

        {
            time_t tp;
            time(&tp);
            // Need to run set_k_time so that kattistime get the current time
            //set_k_time(tp);
            set_k_time(906215504);
            struct tm *t = gmtime(&tp);

            Gregorian g; // dagens datum
            Julian j; // också dagens datum
            //std::cout << "Today it is " << g << " gregorian and " << j << " julian";
            //if (g - j == 0) std::cout << ". It is the same date" << std::endl;
            //g = j;
            //if (g - j == 0) std::cout << "It is still the same date" << std::endl;


            Gregorian gg = j;
            Gregorian ggg(j);
            TS_ASSERT_EQUALS(gg.mod_julian_day(), ggg.mod_julian_day());
            //std:cout << "gg" << gg << endl;
        }

        {
            set_k_time(906215504);
            Julian j, j2;
            j = j;
            j2 = j;

            Gregorian g(j);

            //cout << endl << "g: " <<  g << endl;
            j2.add_year(-3);
            //cout << endl << "j2: " <<  j2 << endl;
        }
        {
            time_t tp;
            time(&tp);
            // Need to run set_k_time so that kattistime get the current time
            set_k_time(tp);
            struct tm *t = gmtime(&tp);

            Gregorian const gZero(1858, 11, 17); // 0 MJD
            Julian const jZero(1858, 11, 5);  // 0 MJD

//            cout << "Will break if day changes! " << endl;
//            Gregorian gToday;
//            Julian jToday;
//            TS_ASSERT_EQUALS(gToday.day(), 9);
//            TS_ASSERT_EQUALS(gToday.month(), 11);
//            TS_ASSERT_EQUALS(gToday.year(), 2013);
//            TS_ASSERT_EQUALS(jToday.day(), 27);
//            TS_ASSERT_EQUALS(jToday.month(), 10);
//            TS_ASSERT_EQUALS(jToday.year(), 2013);

            Julian j(1998, 9, 19);
            j.add_year(-3);
            TS_ASSERT_EQUALS(j.year(), 1995);
            TS_ASSERT_EQUALS(j.month(), 9);
            TS_ASSERT_EQUALS(j.day(), 19);
            j.add_year(3);
            j.add_year(3);
            j.add_year(-3);
            TS_ASSERT_EQUALS(j.year(), 1998);
            TS_ASSERT_EQUALS(j.month(), 9);
            TS_ASSERT_EQUALS(j.day(), 19);
        }
        {
            Gregorian const gZero(1858, 11, 17); // 0 MJD
            Julian const jZero(1858, 11, 5);  // 0 MJD

            TS_ASSERT_EQUALS(gZero.mod_julian_day(), 0);
            TS_ASSERT_EQUALS(jZero.mod_julian_day(), 0);

            Julian j(jZero);
            for(int i = 0; i < 1000; i++) 
                j++;
            TS_ASSERT_EQUALS(j.mod_julian_day(), 1000);

            for(int i = 0; i < 2000; i++) 
                j--;
            TS_ASSERT_EQUALS(j.mod_julian_day(), -1000);

            Gregorian g(gZero);
            for(int i = 0; i < 1000; i++) 
                g++;
            TS_ASSERT_EQUALS(g.mod_julian_day(), 1000);
            for(int i = 0; i < 2000; i++) 
                g--;
            TS_ASSERT_EQUALS(g.mod_julian_day(), -1000);

       
            Gregorian gg = Gregorian(1,1,1);
            Julian jj = Julian(1,1,1);
            gg.set_date_from_mjd(0);
            jj.set_date_from_mjd(0);

            gg = gg;
            jj = jj;

            Date & rj = jj;
            Date & rg = gg;

            for(int i = 0; i < 100000; i++) 
                ++rj;
            TS_ASSERT_EQUALS(jj.mod_julian_day(), 100000);
            for(int i = 0; i < 200000; i++) 
                --rj;
            TS_ASSERT_EQUALS(jj.mod_julian_day(), -100000);

            for(int i = 0; i < 100000; i++) 
                ++rg;
            TS_ASSERT_EQUALS(gg.mod_julian_day(), 100000);
            for(int i = 0; i < 200000; i++) 
                --rg;
            TS_ASSERT_EQUALS(gg.mod_julian_day(), -100000);

            int gv = gg.mod_julian_day();
            Julian tmpj = gg;
            gg = tmpj;
            gg = gg;
            TS_ASSERT_EQUALS(gv, gg.mod_julian_day());

            int jv = jj.mod_julian_day();
            Gregorian tmpg = jj;
            jj = tmpg;
            jj = jj;
            TS_ASSERT_EQUALS(jv, jj.mod_julian_day());
        }

        {
            //Error in operation add_year: "L766:D27;2100-02-29" should have been "L766:D27;2100-02-28"
            //We think we've got a history for this date:
            //gregorian 2104 2 29 @116
            //add_year -4
            Julian j(2004, 2, 10);
            Julian j2(2004, 2, 10);
            Gregorian g(j);
            Gregorian g2(1,1,1);
            g2 = j;

            TS_ASSERT_EQUALS(g.day(), 23);
            TS_ASSERT_EQUALS(g2.day(), 23);

            //Från labbpek:
            //1/1 1900 Greg = 20/12 1899 Jul
            Gregorian gg(1900, 1, 1);
            Julian jj(1,2,3);
            jj = gg;
            jj = jj;
            TS_ASSERT_EQUALS(jj.year(), 1899);
            TS_ASSERT_EQUALS(jj.month(), 12);
            TS_ASSERT_EQUALS(jj.day(), 20);


            Gregorian gg2(1, 2, 3);
            Julian jj2(1899, 12, 20);
            gg2 = jj2;
            gg2 = gg2;
            TS_ASSERT_EQUALS(gg2.year(), 1900);
            TS_ASSERT_EQUALS(gg2.month(), 1);
            TS_ASSERT_EQUALS(gg2.day(), 1);

        }
    }

    void testCalenderPrint(void) {
        Calendar<Gregorian> c;
        c.add_event("Kim fyller", 2, 1, 1989);

        //cout << c;
    }

    void testJulianDayInMonth(void) {
        Julian j(2000, 1, 1);
        Julian j2(2001, 1, 1);
        TS_ASSERT_EQUALS(j.days_in_month(1), 31);
        TS_ASSERT_EQUALS(j.days_in_month(2), 29);
        TS_ASSERT_EQUALS(j2.days_in_month(2), 28);

        TS_ASSERT_EQUALS(j.days_in_month(0), 31);
        TS_ASSERT_EQUALS(j.days_in_month(-1), 30);
        TS_ASSERT_EQUALS(j.days_in_month(13), 31);
        TS_ASSERT_EQUALS(j.days_in_month(14), 29);
    }

    void testCopyAssignment(void) {
        time_t tp;
        time(&tp);
        // Need to run set_k_time so that kattistime get the current time
        set_k_time(tp);
        struct tm *t = gmtime(&tp);
        Gregorian g1(2000, 1, 1);
        Gregorian g2(2000, 4, 4);
        Gregorian g3(g1);

        //2414-11-00
        TS_ASSERT_THROWS_ANYTHING(Julian jul(2414, 11, 0));
        //TS_ASSERT_EQUALS((g1++).day(), 1);
        //TS_ASSERT_EQUALS((++g1).day(), 3);

        //TS_ASSERT_EQUALS((g2--).day(), 4);
        //TS_ASSERT_EQUALS((--g2).day(), 2);

        //TS_ASSERT_EQUALS(g3.year(), 2000);
        //TS_ASSERT_EQUALS(g3.month(), 1);
        //TS_ASSERT_EQUALS(g3.day(), 1);

        Julian j1;
        Date & d1 = j1;
        Julian j2(d1);
        Date * dp = &j2;
        Julian j3(dp);

        ++d1;
        --d1;

        //std::cout << "Today: " << j1 << endl;
    }

    void testGregorianInvalidDate(void) {
        
        TS_ASSERT_THROWS(Gregorian g1(-1,1,2), std::out_of_range);
        TS_ASSERT_THROWS(Gregorian g2(2001, 2, 29), std::out_of_range);
        TS_ASSERT_THROWS_NOTHING(Gregorian g3(2000, 2, 29));
        TS_ASSERT_THROWS_NOTHING(Gregorian g4(2001, 12, 31));
        TS_ASSERT_THROWS_NOTHING(Gregorian g5(2001, 1, 1));
        TS_ASSERT_THROWS(Gregorian g6(2001, 4, 31), std::out_of_range);

    }

    void testJulianDayName(void) {
    }

    void testGregorianNameDay(void) {
        std::stringstream ss;
        Gregorian g1(2013, 11, 7);
        ss << g1.week_day_name();
        TS_ASSERT_EQUALS("thursday", ss.str());
    }

    void testGregorianMinusMinus(void) {
        Gregorian g1(1999, 1, 1);
        g1--;
        TS_ASSERT_EQUALS(g1.day(), 31);
        TS_ASSERT_EQUALS(g1.month(), 12);
        TS_ASSERT_EQUALS(g1.year(), 1998);

        Gregorian g2(1999, 1, 31);
        g2--;
        TS_ASSERT_EQUALS(g2.day(), 30);
        TS_ASSERT_EQUALS(g2.month(), 1);

        Gregorian g3(1999, 3, 1);
        g3--;
        TS_ASSERT_EQUALS(g3.day(), 28);
        TS_ASSERT_EQUALS(g3.month(), 2);

        Gregorian g4(2000, 3, 1);
        g4--;
        TS_ASSERT_EQUALS(g4.day(), 29);
        TS_ASSERT_EQUALS(g4.month(), 2);
    }

    void testGregorianPlusPlus(void) {
        Gregorian g1(1999, 1, 1);
        g1++;
        TS_ASSERT_EQUALS(g1.day(), 2);

        Gregorian g2(1999, 1, 31);
        g2++;
        TS_ASSERT_EQUALS(g2.day(), 1);
        TS_ASSERT_EQUALS(g2.month(), 2);

        Gregorian g3(1999, 2, 28);
        g3++;
        TS_ASSERT_EQUALS(g3.day(), 1);
        TS_ASSERT_EQUALS(g3.month(), 3);

        Gregorian g4(2000, 2, 28);
        g4++;
        g4++;
        TS_ASSERT_EQUALS(g4.day(), 1);
        TS_ASSERT_EQUALS(g4.month(), 3);
    }

    void testJulianAddYear(void) {
        Julian j1(1999, 1, 1);
        j1.add_year();
        TS_ASSERT_EQUALS(j1.year(), 2000);
        TS_ASSERT_EQUALS(j1.month(), 1);
        TS_ASSERT_EQUALS(j1.day(), 1);

        Julian j2(2000, 2, 29);
        j2.add_year();
        TS_ASSERT_EQUALS(j2.year(), 2001);
        TS_ASSERT_EQUALS(j2.month(), 2);
        TS_ASSERT_EQUALS(j2.day(), 28);
        
        Julian j3(2000, 2, 29);
        j3.add_year(4);
        TS_ASSERT_EQUALS(j3.year(), 2004);
        TS_ASSERT_EQUALS(j3.month(), 2);
        TS_ASSERT_EQUALS(j3.day(), 29);

        Julian j4(2000, 5, 5);
        j4.add_year(4);
        TS_ASSERT_EQUALS(j4.year(), 2004);
        j4.add_year(10);
        TS_ASSERT_EQUALS(j4.year(), 2014);
        j4.add_year(1);
        TS_ASSERT_EQUALS(j4.year(), 2015);
        j4.add_year(3);
        TS_ASSERT_EQUALS(j4.year(), 2018);

        Julian j5(2000, 3, 29);
        j5.add_year(1);
        TS_ASSERT_EQUALS(j5.year(), 2001);
        TS_ASSERT_EQUALS(j5.day(), 29);
    }

    void testJulianDaysThisMonth(void) {
        Julian j1(1999, 1, 1);
        TS_ASSERT_EQUALS(j1.days_this_month(), 31);

        Julian j2(1999, 2, 1);
        TS_ASSERT_EQUALS(j2.days_this_month(), 28);

        Julian j3(2000, 2, 1);
        TS_ASSERT_EQUALS(j3.days_this_month(), 29);

        Julian j4(2004, 2, 1);
        TS_ASSERT_EQUALS(j4.days_this_month(), 29);

        Julian j5(2004, 12, 1);
        TS_ASSERT_EQUALS(j5.days_this_month(), 31);
    }

    void testJulianAddDay(void) {

        Julian j1(2000, 1, 1);
        j1.add_day();
        TS_ASSERT_EQUALS(j1.day(), 2);

        Julian j2(2000, 1, 31);
        j2.add_day();
        TS_ASSERT_EQUALS(j2.day(), 1);
        TS_ASSERT_EQUALS(j2.month(), 2);

        Julian j3(2000, 2, 28);
        j3.add_day();
        TS_ASSERT_EQUALS(j3.day(), 29);
        TS_ASSERT_EQUALS(j3.month(), 2);

        Julian j4(2000, 2, 29);
        j4.add_day();
        TS_ASSERT_EQUALS(j4.day(), 1);
 
        Julian j5(1999, 2, 28);
        j5.add_day();
        TS_ASSERT_EQUALS(j5.day(), 1);
        TS_ASSERT_EQUALS(j5.month(), 3);
 
        Julian j6(1999, 12, 31);
        j6.add_day();
        TS_ASSERT_EQUALS(j6.day(), 1);
        TS_ASSERT_EQUALS(j6.month(), 1);
        TS_ASSERT_EQUALS(j6.year(), 2000);
 
        Julian j7(1999, 12, 31);
        j7.add_day(2);
        TS_ASSERT_EQUALS(j7.day(), 2);
        TS_ASSERT_EQUALS(j7.month(), 1);
        TS_ASSERT_EQUALS(j7.year(), 2000);

        Julian j8(1999, 1, 1);
        j8.add_day(2);
        TS_ASSERT_EQUALS(j8.day(), 3);
        TS_ASSERT_EQUALS(j8.month(), 1);
        TS_ASSERT_EQUALS(j8.year(), 1999);
        
        Julian j9(1999, 1, 30);
        j9.add_day(2);
        TS_ASSERT_EQUALS(j9.day(), 1);
        TS_ASSERT_EQUALS(j9.month(), 2);
        TS_ASSERT_EQUALS(j9.year(), 1999);
    }

    void testJulianAddMonth(void) {

        Julian j1(2000, 1, 1);
        j1.add_month();
        TS_ASSERT_EQUALS(j1.year(), 2000);
        TS_ASSERT_EQUALS(j1.month(), 2);
        TS_ASSERT_EQUALS(j1.day(), 1);

        Julian j2(2000, 3, 31);
        j2.add_month();
        TS_ASSERT_EQUALS(j2.year(), 2000);
        TS_ASSERT_EQUALS(j2.month(), 4);
        TS_ASSERT_EQUALS(j2.day(), 30);

        Julian j3(2000, 12, 31);
        j3.add_month();
        TS_ASSERT_EQUALS(j3.year(), 2001);
        TS_ASSERT_EQUALS(j3.month(), 1);
        TS_ASSERT_EQUALS(j3.day(), 31);

        // Mijht skip entire Frebruary check
        // with leapyear
        Julian j4(2000, 1, 31);
        j4.add_month();
        TS_ASSERT_EQUALS(j4.year(), 2000);
        TS_ASSERT_EQUALS(j4.month(), 3);
        TS_ASSERT_EQUALS(j4.day(), 1);

        // without leapyear
        Julian j5(2001, 1, 31);
        j5.add_month();
        TS_ASSERT_EQUALS(j5.year(), 2001);
        TS_ASSERT_EQUALS(j5.month(), 3);
        TS_ASSERT_EQUALS(j5.month(), 3);
        TS_ASSERT_EQUALS(j5.day(), 2);

        Julian j6(2001, 3, 10);
        j6.add_month();
        TS_ASSERT_EQUALS(j6.month(), 4);
        j6.add_month(12);
        TS_ASSERT_EQUALS(j6.month(), 4);
        j6.add_month(6);
        TS_ASSERT_EQUALS(j6.month(), 10);

        j6.add_month(-1);
        TS_ASSERT_EQUALS(j6.month(), 9);
        j6.add_month(-12);
        TS_ASSERT_EQUALS(j6.month(), 9);
      j6.add_month(-6);
        TS_ASSERT_EQUALS(j6.month(), 3);

        Julian j7(2001, 3, 29);
        j7.add_month(-1);
        TS_ASSERT_EQUALS(j7.month(), 2);
        TS_ASSERT_EQUALS(j7.day(), 27);

        Julian j8(2000, 3, 29);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.month(), 2);
        TS_ASSERT_EQUALS(j8.day(), 29);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.month(), 1);
        TS_ASSERT_EQUALS(j8.day(), 29);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.month(), 12);
        TS_ASSERT_EQUALS(j8.day(), 29);
        j8.add_month(-10);
        TS_ASSERT_EQUALS(j8.month(), 2);
        TS_ASSERT_EQUALS(j8.day(), 27);
        TS_ASSERT_EQUALS(j8.year(), 1999);
        //j8.add_month(-12);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        TS_ASSERT_EQUALS(j8.month(), 1);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.month(), 12);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        j8.add_month(-1);
        TS_ASSERT_EQUALS(j8.day(), 27);
        TS_ASSERT_EQUALS(j8.year(), 1998);
    }
    
    // --------------

    void testGregorianAddYear(void) {
        Gregorian g1(1999, 1, 1);
        g1.add_year();
        TS_ASSERT_EQUALS(g1.year(), 2000);
        TS_ASSERT_EQUALS(g1.month(), 1);
        TS_ASSERT_EQUALS(g1.day(), 1);

        Gregorian g2(2000, 2, 29);
        g2.add_year();
        TS_ASSERT_EQUALS(g2.year(), 2001);
        TS_ASSERT_EQUALS(g2.month(), 2);
        TS_ASSERT_EQUALS(g2.day(), 28);
        
        Gregorian g3(2000, 2, 29);
        g3.add_year(4);
        TS_ASSERT_EQUALS(g3.year(), 2004);
        TS_ASSERT_EQUALS(g3.month(), 2);
        TS_ASSERT_EQUALS(g3.day(), 29);

        Gregorian g4(2000, 2, 29);
        g4.add_year(-4);
        TS_ASSERT_EQUALS(g4.year(), 1996);
        TS_ASSERT_EQUALS(g4.month(), 2);
        TS_ASSERT_EQUALS(g4.day(), 29);

        Gregorian g5(2000, 2, 29);
        g5.add_year(-1);
        TS_ASSERT_EQUALS(g5.year(), 1999);
        TS_ASSERT_EQUALS(g5.month(), 2);
        TS_ASSERT_EQUALS(g5.day(), 28);
    }

    void testGregorianDaysThisMonth(void) {
        Gregorian g1(1999, 1, 1);
        TS_ASSERT_EQUALS(g1.days_this_month(), 31);

        Gregorian g2(1999, 2, 1);
        TS_ASSERT_EQUALS(g2.days_this_month(), 28);

        Gregorian g3(2000, 2, 1);
        TS_ASSERT_EQUALS(g3.days_this_month(), 29);

        Gregorian g4(2004, 2, 1);
        TS_ASSERT_EQUALS(g4.days_this_month(), 29);

        Gregorian g5(2004, 12, 1);
        TS_ASSERT_EQUALS(g5.days_this_month(), 31);
    }

    void testGregorianAddDay(void) {

        Gregorian g1(2000, 1, 1);
        g1.add_day();
        TS_ASSERT_EQUALS(g1.day(), 2);

        Gregorian g2(2000, 1, 31);
        g2.add_day();
        TS_ASSERT_EQUALS(g2.day(), 1);
        TS_ASSERT_EQUALS(g2.month(), 2);

        Gregorian g3(2000, 2, 28);
        g3.add_day();
        TS_ASSERT_EQUALS(g3.day(), 29);
        TS_ASSERT_EQUALS(g3.month(), 2);

        Gregorian g4(2000, 2, 29);
        g4.add_day();
        TS_ASSERT_EQUALS(g4.day(), 1);
 
        Gregorian g5(1999, 2, 28);
        g5.add_day();
        TS_ASSERT_EQUALS(g5.day(), 1);
        TS_ASSERT_EQUALS(g5.month(), 3);
 
        Gregorian g6(1999, 12, 31);
        g6.add_day();
        TS_ASSERT_EQUALS(g6.day(), 1);
        TS_ASSERT_EQUALS(g6.month(), 1);
        TS_ASSERT_EQUALS(g6.year(), 2000);
 
        Gregorian g7(1999, 12, 31);
        g7.add_day(2);
        TS_ASSERT_EQUALS(g7.day(), 2);
        TS_ASSERT_EQUALS(g7.month(), 1);
        TS_ASSERT_EQUALS(g7.year(), 2000);

        Gregorian g8(1999, 1, 1);
        g8.add_day(2);
        TS_ASSERT_EQUALS(g8.day(), 3);
        TS_ASSERT_EQUALS(g8.month(), 1);
        TS_ASSERT_EQUALS(g8.year(), 1999);
        
        Gregorian g9(1999, 1, 30);
        g9.add_day(2);
        TS_ASSERT_EQUALS(g9.day(), 1);
        TS_ASSERT_EQUALS(g9.month(), 2);
        TS_ASSERT_EQUALS(g9.year(), 1999);
    }

    void testGregorianAddMonth(void) {

        Gregorian g1(2000, 1, 1);
        g1.add_month();
        TS_ASSERT_EQUALS(g1.year(), 2000);
        TS_ASSERT_EQUALS(g1.month(), 2);
        TS_ASSERT_EQUALS(g1.day(), 1);

        Gregorian g2(2000, 3, 31);
        g2.add_month();
        TS_ASSERT_EQUALS(g2.year(), 2000);
        TS_ASSERT_EQUALS(g2.month(), 4);
        TS_ASSERT_EQUALS(g2.day(), 30);

        Gregorian g3(2000, 12, 31);
        g3.add_month();
        TS_ASSERT_EQUALS(g3.year(), 2001);
        TS_ASSERT_EQUALS(g3.month(), 1);
        TS_ASSERT_EQUALS(g3.day(), 31);

        // Might skip entire Frebruary check
        // with leapyear
        Gregorian g4(2000, 1, 31);
        g4.add_month();
        TS_ASSERT_EQUALS(g4.year(), 2000);
        TS_ASSERT_EQUALS(g4.month(), 3);
        TS_ASSERT_EQUALS(g4.day(), 1);

        // without leapyear
        Gregorian g5(2001, 1, 31);
        g5.add_month();
        TS_ASSERT_EQUALS(g5.year(), 2001);
        TS_ASSERT_EQUALS(g5.month(), 3);
        TS_ASSERT_EQUALS(g5.day(), 2);

        // without leapyear
        Gregorian g6(2001, 5, 1);
        g6.add_month(-1);
        TS_ASSERT_EQUALS(g6.year(), 2001);
        TS_ASSERT_EQUALS(g6.month(), 4);
        TS_ASSERT_EQUALS(g6.day(), 1);

        // without leapyear
        Gregorian g7(2001, 1, 1);
        g7.add_month(-1);
        TS_ASSERT_EQUALS(g7.year(), 2000);
        TS_ASSERT_EQUALS(g7.month(), 12);
        TS_ASSERT_EQUALS(g7.day(), 1);

        Gregorian g8(2000, 2, 29);
        g8.add_month(-1);
        TS_ASSERT_EQUALS(g8.year(), 2000);
        TS_ASSERT_EQUALS(g8.month(), 1);
        TS_ASSERT_EQUALS(g8.day(), 29);

        Gregorian g9(2001, 4, 13);
        g9.add_month(-3);
        TS_ASSERT_EQUALS(g9.year(), 2001);
        TS_ASSERT_EQUALS(g9.month(), 1);
        TS_ASSERT_EQUALS(g9.day(), 13);

        Gregorian g10(2001, 4, 13);
        g10.add_month(4);
        TS_ASSERT_EQUALS(g10.year(), 2001);
        TS_ASSERT_EQUALS(g10.month(), 8);
        TS_ASSERT_EQUALS(g10.day(), 13);
    }

    void testCalendarCopyConstructor(void) {
        Calendar<Gregorian> c;
        c.set_date(2013, 4, 15);
        c.add_event("old", 20, 4, 1900);
        c.add_event("old2", 24, 4, 1900);

        Calendar<Julian> j(c);
        TS_ASSERT_EQUALS(j.m_events.size(), 2);

        Calendar<Gregorian> c2(c);
        TS_ASSERT_EQUALS(c2.m_events.size(), 2);
    }

    void testCalendarAddRemoveGregorian(void) {
        Calendar<Gregorian> c;
        
        c.set_date(2013, 4, 15);
        c.add_event("old", 20, 4, 1900);
        c.add_event("today");
        c.add_event("today1", 15, 4, 2013);
        c.add_event("tommorrow", 16, 4, 2013);
        c.add_event("christmas", 24, 12, 2013);
        c.add_event("lucia", 13, 12, 2013);
        TS_ASSERT_EQUALS(c.m_events.size(), 6);

        TS_ASSERT_EQUALS(c.remove_event("old"), false);
        TS_ASSERT_EQUALS(c.m_events.size(), 6);
        TS_ASSERT_EQUALS(c.remove_event("old", 20, 4, 1900), true);
        TS_ASSERT_EQUALS(c.m_events.size(), 5);
        TS_ASSERT_EQUALS(c.remove_event("old", 20, 4, 1900), false);
        TS_ASSERT_EQUALS(c.m_events.size(), 5);
        TS_ASSERT_EQUALS(c.remove_event("lucia"), false);
        TS_ASSERT_EQUALS(c.m_events.size(), 5);
        c.set_date(2013, 12, 13);
        TS_ASSERT_EQUALS(c.remove_event("lucia"), true);
        TS_ASSERT_EQUALS(c.m_events.size(), 4);

        TS_ASSERT_EQUALS(c.add_event("lucia", 13, 12, 2013), true);
        TS_ASSERT_EQUALS(c.add_event("lucia", 13, 12, 2013), false);
        TS_ASSERT_EQUALS(c.add_event("lucia", 13, 12), false);
        TS_ASSERT_EQUALS(c.add_event("lucia", 13), false);
        TS_ASSERT_EQUALS(c.add_event("lucia"), false);
        TS_ASSERT_EQUALS(c.m_events.size(), 5);

        Calendar<Julian> jc;
        jc = c;
        TS_ASSERT_EQUALS(jc.m_events.size(), 5);
        
    }

    void testCalendarSetDate(void) {
        Calendar<Julian> c;

        c.set_date(2001, 2, 2);
        TS_ASSERT_EQUALS(c.m_current_date.year(), 2001);
        c.set_date(2002, 2, 2);
        TS_ASSERT_EQUALS(c.m_current_date.year(), 2002);

        TS_ASSERT_EQUALS(c.set_date(1990, 2, 34), false);
        TS_ASSERT_EQUALS(c.set_date(-1990, 2, 4), false);

        Calendar<Gregorian> gc;
        TS_ASSERT_EQUALS(gc.set_date(2035, 8, 18), true);
        TS_ASSERT_EQUALS(c.set_date(2035, 8, 18), true);

    }

    void testCalendarAssignment(void) {
        Calendar<Gregorian> kims;
        Calendar<Gregorian> ottos;
        Calendar<Julian> linus;

        kims.add_event("birthsday", 20, 1, 1989);

        // inga dubletter
        TS_ASSERT_EQUALS(kims.add_event("birthsday", 20, 1, 1989), false);

        /*
        ottos = kims;
        TS_ASSERT_EQUALS(ottos.m_events.size(), 1);

        ottos.add_event("hockeymatch", 3, 2, 1989);
        TS_ASSERT_EQUALS(ottos.m_current_date.mod_julian_day(), kims.m_current_date.mod_julian_day());
        TS_ASSERT_EQUALS(ottos.m_events.size(), 2);
        TS_ASSERT_EQUALS(kims.m_events.size(), 1);

        linus = kims; 
        TS_ASSERT_EQUALS(linus.m_events.size(), 1);
        TS_ASSERT_EQUALS(linus.m_events[0].m_date.day(), 7);
        TS_ASSERT_EQUALS(linus.m_events[0].m_date.month(), 1);
        TS_ASSERT_EQUALS(linus.m_events[0].m_date.year(), 1989);

        TS_ASSERT_EQUALS(kims.remove_event("birthsday", 20, 1, 1989), true);
        TS_ASSERT_EQUALS(kims.remove_event("birthsday", 20, 1, 1989), false);
        */
    }

    void testCalendar(void) {
        time_t tp;
        time(&tp);
        // Need to run set_k_time so that kattistime get the current time
        set_k_time(tp);
        struct tm *t = gmtime(&tp);

        Calendar<Gregorian> cal;

        TS_ASSERT_EQUALS(cal.year(), t->tm_year + 1900);
        TS_ASSERT_EQUALS(cal.month(), t->tm_mon + 1);
        TS_ASSERT_EQUALS(cal.day(), t->tm_mday);

        Calendar<Julian> jcal;
        Julian today_j(jcal.year(), jcal.month(), jcal.day());
        Gregorian today_g(1,2,3);
        today_g = today_j;
        TS_ASSERT_EQUALS(today_g.year(), t->tm_year + 1900);
        TS_ASSERT_EQUALS(today_g.month(), t->tm_mon + 1);
        TS_ASSERT_EQUALS(today_g.day(), t->tm_mday);

        Calendar<Gregorian> c;
        TS_ASSERT_EQUALS(c.add_event("kim fyller år", 2,1,2014), true);
        TS_ASSERT_EQUALS(c.add_event("kim skriver en bok"), true);
        TS_ASSERT_EQUALS(c.add_event("kim pillar nosen", 2), true);
        TS_ASSERT_EQUALS(c.add_event("kim pillar nosen2", 1, 2), true);
        TS_ASSERT_EQUALS(c.add_event("kim spelar hockey", 31, 2, 2000), false); // Invalid date!
        TS_ASSERT_EQUALS(c.add_event("kim spelar hockey", 21, 2, 2000), true);


        TS_ASSERT_EQUALS(c.add_event("kim spelar hockey", 21, 2, -1), false);

        // KATTIS: Missing date 1974-02-07 : ywrXjGV
        TS_ASSERT_EQUALS(c.add_event("ywrXjGV", 7, 2, 1974), true);
        TS_ASSERT_EQUALS(c.set_date(1974, 2, 6), true);
        TS_ASSERT_EQUALS(c.m_current_date.year(), 1974);
        TS_ASSERT_EQUALS(c.m_current_date.month(), 2);
        TS_ASSERT_EQUALS(c.m_current_date.day(), 6);
        //cout << "--------" << endl;
        //cout << c;
    }

    void testOutput(void) {
        std::stringstream ss;
        Gregorian g1(2400,2,3);
        Julian j1(2000,2,3);
        Julian j2(1,2,3);

        ss << g1;
        TS_ASSERT_EQUALS(ss.str(), "2400-02-03");
        ss.str("");

        ss << j1;
        TS_ASSERT_EQUALS(ss.str(), "2000-02-03");
        ss.str("");

        ss << j2;
        TS_ASSERT_EQUALS(ss.str(), "0001-02-03");
        ss.str("");
    }

    void testGregorianLeapYear(void) {
        Gregorian g1(2400,2,3);
        TS_ASSERT_EQUALS(g1.leap_year(), true);
        Gregorian g2(2100,2,3);
        TS_ASSERT_EQUALS(g2.leap_year(), false);
        Gregorian g3(2404,2,3);
        TS_ASSERT_EQUALS(g3.leap_year(), true);
        Gregorian g4(2405,2,3);
        TS_ASSERT_EQUALS(g4.leap_year(), false);
        Gregorian g5(2300,2,3);
        TS_ASSERT_EQUALS(g5.leap_year(), false);
    }

    void testJulianLeapYear(void) {
        Julian j1(2000,2,3);
        TS_ASSERT_EQUALS(j1.leap_year(), true);
        Julian j2(2001,2,3);
        TS_ASSERT_EQUALS(j2.leap_year(), false);
        Julian j3(1999,2,3);
        TS_ASSERT_EQUALS(j3.leap_year(), false);
    }

    void testGregorianToJulianAssigment(void) {
        
        // Just date some date conversoins which we know
        // are true (according to some date site which we don't really trust).
        Gregorian g1(2013, 10, 20);
        Julian j1(1,2,3);
        j1 = g1;
        TS_ASSERT_EQUALS(j1.year(), 2013);
        TS_ASSERT_EQUALS(j1.month(), 10);
        TS_ASSERT_EQUALS(j1.day(), 7);

        Gregorian g2(2500, 1, 1);
        Julian j2(1,2,3);
        j2 = g2;
        TS_ASSERT_EQUALS(j2.year(), 2499);
        TS_ASSERT_EQUALS(j2.month(), 12);
        TS_ASSERT_EQUALS(j2.day(), 16);

        Gregorian g3(1880, 12, 31);
        Julian j3(1,2,3);
        j3 = g3;
        TS_ASSERT_EQUALS(j3.year(), 1880);
        TS_ASSERT_EQUALS(j3.month(), 12);
        TS_ASSERT_EQUALS(j3.day(), 19);
    }

    void testJulianToGregorianAssigment(void) {
        
        // Just date some date conversoins which we know
        // are true (according to some date site which we don't really trust).
        Julian j1(2013, 10, 7);
        Gregorian g1(1, 2, 3);
        g1 = j1;
        TS_ASSERT_EQUALS(g1.year(), 2013);
        TS_ASSERT_EQUALS(g1.month(), 10);
        TS_ASSERT_EQUALS(g1.day(), 20);

        Julian j2(2499, 12, 16);
        Gregorian g2(1, 2, 3);
        g2 = j2;
        TS_ASSERT_EQUALS(g2.year(), 2500);
        TS_ASSERT_EQUALS(g2.month(), 1);
        TS_ASSERT_EQUALS(g2.day(), 1);

        Julian j3(1880, 12, 19);
        Gregorian g3(1, 2, 3);
        g3 = j3;
        TS_ASSERT_EQUALS(g3.year(), 1880);
        TS_ASSERT_EQUALS(g3.month(), 12);
        TS_ASSERT_EQUALS(g3.day(), 31);
    }

    void testSetDateFromMjd(void) {
        Gregorian g1(2013, 10, 20);
        g1.set_date_from_mjd(g1.mod_julian_day());
        TS_ASSERT_EQUALS(g1.year(), 2013);
        TS_ASSERT_EQUALS(g1.month(), 10);
        TS_ASSERT_EQUALS(g1.day(), 20);

        Gregorian g2(2558, 10, 20);
        g2.set_date_from_mjd(g2.mod_julian_day());
        TS_ASSERT_EQUALS(g2.year(), 2558);
        TS_ASSERT_EQUALS(g2.month(), 10);
        TS_ASSERT_EQUALS(g2.day(), 20);

        Gregorian g3(1859, 10, 20);
        g3.set_date_from_mjd(g3.mod_julian_day());
        TS_ASSERT_EQUALS(g3.year(), 1859);
        TS_ASSERT_EQUALS(g3.month(), 10);
        TS_ASSERT_EQUALS(g3.day(), 20);

        Julian j1(2013, 10, 20);
        j1.set_date_from_mjd(j1.mod_julian_day());
        TS_ASSERT_EQUALS(j1.year(), 2013);
        TS_ASSERT_EQUALS(j1.month(), 10);
        TS_ASSERT_EQUALS(j1.day(), 20);

        Julian j2(2558, 10, 20);
        j2.set_date_from_mjd(j2.mod_julian_day());
        TS_ASSERT_EQUALS(j2.year(), 2558);
        TS_ASSERT_EQUALS(j2.month(), 10);
        TS_ASSERT_EQUALS(j2.day(), 20);

        Julian j3(1859, 10, 20);
        j3.set_date_from_mjd(j3.mod_julian_day());
        TS_ASSERT_EQUALS(j3.year(), 1859);
        TS_ASSERT_EQUALS(j3.month(), 10);
        TS_ASSERT_EQUALS(j3.day(), 20);
    }

    // Difference between same date should be about 13.
    void testMJDDiff(void) {
        Julian      j1(2013, 10, 20);
        Gregorian   g1(2013, 10, 20);
        TS_ASSERT_EQUALS(g1 - j1, -13);
        TS_ASSERT_EQUALS(j1 - g1, 13);

        Gregorian g2(1940, 5, 4);
        Julian    j2(1940, 4, 21);
        TS_ASSERT_EQUALS(g2 - j2, 0);
        TS_ASSERT_EQUALS(j2 - g2, 0);

        Gregorian g3(2130, 9, 28);
        Julian    j3(2130, 9, 14);
        TS_ASSERT_EQUALS(g3 - j3, 0);
        TS_ASSERT_EQUALS(j3 - g3, 0);

    }

    void testJulianMJD(void) {
        int mjd_j1 = 56598;
        Julian  j1(2013, 10, 20);
        TS_ASSERT_EQUALS(j1.mod_julian_day(), mjd_j1);

        int mjd_j2 = 15;
        Julian  j2(1858, 11, 20);
        TS_ASSERT_EQUALS(j2.mod_julian_day(), mjd_j2);

        int mjd_j3 = 135766;
        Julian  j3(2230, 7, 21);
        TS_ASSERT_EQUALS(j3.mod_julian_day(), mjd_j3);

        int mjd_j4 = 255367;
        Julian  j4(2558, 1, 1);
        TS_ASSERT_EQUALS(j4.mod_julian_day(), mjd_j4);

        // Kattis testar denna.
        Julian  j5(1858, 1, 1);
        TS_ASSERT_EQUALS(j5.mod_julian_day(), -308);

        // Kattis testar denna.
        Julian  j6(1858, 11, 5);
        TS_ASSERT_EQUALS(j6.mod_julian_day(), 0);

        Julian  j7(1858, 11, 4);
        TS_ASSERT_EQUALS(j7.mod_julian_day(), -1);

        Julian  j9(1858, 11, 3);
        TS_ASSERT_EQUALS(j9.mod_julian_day(), -2);

        Julian  j8(1858, 11, 6);
        TS_ASSERT_EQUALS(j8.mod_julian_day(), 1);

    }

    void testGregorianMJD(void) {
        // 17 november 1858
        int mjd = 0;
        Gregorian g(1858, 11, 17);
        TS_ASSERT_EQUALS(g.mod_julian_day(), mjd);

        int mjd_g1 = 56585;
        Gregorian   g1(2013, 10, 20);
        TS_ASSERT_EQUALS(g1.mod_julian_day(), mjd_g1);

        int mjd_g2 = 31533;
        Gregorian   g2(1945, 3, 19);
        TS_ASSERT_EQUALS(g2.mod_julian_day(), mjd_g2);

        int mjd_g3 = 88217;
        Gregorian   g3(2100, 5, 29);
        TS_ASSERT_EQUALS(g3.mod_julian_day(), mjd_g3);
        
        int mjd_g4 = 411533;
        Gregorian   g4(2985, 8, 13);
        TS_ASSERT_EQUALS(g4.mod_julian_day(), mjd_g4);

        // 1858 11 16 = -1, enligt kattis
        Gregorian   g5(1858, 11, 16);
        TS_ASSERT_EQUALS(g5.mod_julian_day(), -1);

        Gregorian   g6(1858, 11, 17);
        TS_ASSERT_EQUALS(g6.mod_julian_day(), 0);
    }

    void testAssignment(void) {
    }

    void testCreate(void) {
        using namespace lab2;

        // Make sure default constructors work
        Gregorian gg();
        Julian    jj();

        // Make sure its possible to create with year, month, day constructor.
        Gregorian g(1,2,3);
        TS_ASSERT_EQUALS(g.year(), 1);
        TS_ASSERT_EQUALS(g.month(), 2);
        TS_ASSERT_EQUALS(g.day(), 3);

        Julian    j(4,5,6);
        TS_ASSERT_EQUALS(j.year(), 4);
        TS_ASSERT_EQUALS(j.month(), 5);
        TS_ASSERT_EQUALS(j.day(), 6);
    }

    //void testTest(void) {
    //    std::cout << std::endl << "Testen fungerar." << std::endl;
    //}
};
#endif
