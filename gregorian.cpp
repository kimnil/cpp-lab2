#include "gregorian.h"
#include <iostream>
#include <iomanip>
#include "kattistime.h"
#include <stdexcept>      // std::out_of_range

#define DEBUG 0

// Skip lab1:: everywhere.
using namespace lab2;

const int Gregorian::DAYS_PER_MONTH[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const int Gregorian::MONTHS_PER_YEAR = 12;
const std::string Gregorian::MONTH_NAMES[12] = {
                                    std::string("january"),
                                    std::string("february"),
                                    std::string("march"),
                                    std::string("april"),
                                    std::string("may"),
                                    std::string("june"),
                                    std::string("july"),
                                    std::string("august"),
                                    std::string("september"),
                                    std::string("october"),
                                    std::string("november"),
                                    std::string("december")
                                   };

const std::string Gregorian::DAY_NAMES[7] = {
                                    std::string("monday"),
                                    std::string("tuesday"),
                                    std::string("wednesday"),
                                    std::string("thursday"),
                                    std::string("friday"),
                                    std::string("saturday"),
                                    std::string("sunday")
                                   };

//enum Gregorian::MONTHS {JANUARY, FEBRUARY};

/*
const enum MONTHS = { 
                            JANUARY = 1,
                            FEBRUARY = 2
                           };
                           */
/**
  * This is the default constructor.
  */
Gregorian::Gregorian(void)
    : Date()  {
    if (DEBUG) {
        std::cout << std::endl << "Gregorian::Gregorian(void) - default konstruktorn" << std::endl;
    } 

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;
    
    time_t mytime;
    k_time(&mytime);

    struct tm *t = gmtime(&mytime);

    m_year = t->tm_year + 1900;
    m_month = t->tm_mon + 1;
    m_day = t->tm_mday;
}

/**
  * Year, month, day constructor
  */
Gregorian::Gregorian(int year, int month, int day) 
    : Date(year, month, day) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    // Make sure it's a valid date!
    if( (day <= 0 || month <= 0 || year < 0) ||
        day > 31 || month > 12) {
        throw std::out_of_range("no such date!");
    }

    if (day > days_in_month(month)) {
        throw std::out_of_range("no such date!");
    }
           
    if (DEBUG) {
        std::cout << std::endl << "Gregorian::Gregorian(int, int, int)" << std::endl;
    } 
}

/**
  * Default copy constructor.
  */
Gregorian::Gregorian(const Gregorian& g) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;
    *this = g;
}

/**
  * This is copy constructor.
  */
Gregorian::Gregorian(const Date & rhs) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    *this = rhs;
}

/**
  * Construct from date pointer (conversion constructor).
  */
Gregorian::Gregorian(Date const * const dp) {

    // Set some calender specific stuff.
    m_days_per_week = 7;
    m_months_per_year = 12;

    *this = *dp;
}

/**
  * Deconstructor.
 */
Gregorian::~Gregorian(void) {
    if (DEBUG) {
        std::cout << std::endl << "Gregorian::~Gregorian(void) - default dekonstruktorn" << std::endl;
    } 
}

/**
  * Returns the number of days in some month.
  * parameter month is 1-indexed so 1 = JANUARY
              also, input will be 12-modded so 15 would be 3 = march
 */
int Gregorian::days_in_month(int month) const {
   month = month - 1; // 0 index
   month = (month + months_per_year()) % months_per_year(); // make sure -1 is 11 etc;

    // From now on month is 0 indexed, hence FEBRUARY == 1
    if (leap_year() && month == 1) {
        return 29;
    } else {
        return DAYS_PER_MONTH[month]; // Note, add one
    }
}

/**
  * Returns the number of days in the current month.
 */
int Gregorian::days_this_month(void) const {
    return days_in_month(m_month);
}

/**
  * Return the name of the current day, eg, "wednesday"
  */
std::string Gregorian::week_day_name(void) const {
    return DAY_NAMES[(mod_julian_day() + 2400001) % 7];
}

/**
  * Return the name of the current month, eg, "january"
  */
std::string Gregorian::month_name(void) const {
    return MONTH_NAMES[m_month - 1];
}

/**
  * Returns the number of day from this day to MJD.
  * Algorithm source: http://en.wikipedia.org/wiki/Julian_day
  */
int Gregorian::mod_julian_day(void) const {
    int a, y, m, jd, res;

    a = ((14 - m_month) / 12);
    y = m_year + 4800 - a;
    m = m_month + 12*a - 3;

    jd = m_day + (153*m + 2) / 5  + 365*y + y/4 - y/100 + y/400 - 32045;

    res = jd - 2400000.5;
    // fulhack för att fixa MJD blir ett för mycket vid negativa tal, innan datumet nedan
    if (res < 0 || (m_year == 1858 && m_month <= 11 && m_day < 17)) {
        res--;
    }

    return res;
}


void Gregorian::set_date_from_mjd(const int mjd) {
    int f, e, g, h, D, M, Y, J;
    int y, j, m, n, r, p, v, u, s, w, B, C;

    J = mjd + 2400001;
       
    y = 4716; j = 1401; m = 2; n = 12; r = 4; p = 1461;
    v = 3; u = 5; s = 153; w = 2; B = 274277; C = -38;

    f = J + j + (((4 * J + B)/146097) * 3)/4 + C;
    e = r * f + v;
    g = (e % p)/r;
    h = u * g + w;
    D = (h % s)/u + 1;
    M = ((h/s + m) % n) + 1;
    Y = e/p - y + (n + m - M)/n;

    m_year = Y;
    m_month = M;
    m_day = D;
}

/**
  * Override default Assignment operator.
  */
Gregorian& Gregorian::operator=(Gregorian const& rhs) {
    /*
    int mjd = rhs.mod_julian_day();
    this->set_date_from_mjd(mjd);
    */
    Date::operator=(rhs);

    return (*this);
}

/**
  * Increase day by one.
  */
Gregorian Gregorian::operator++(int dummy) {
    Gregorian tmp(*this);
    this->add_day(1);
    return tmp;
}

/**
  * Increase day by one.
  */
Gregorian Gregorian::operator--(int dummy) {
    Gregorian tmp(*this);
    this->add_day(-1);
    return tmp;
}

/**
  * Returns true if current year is a leap year.
  */
bool Gregorian::leap_year(int n) const {
    int year = m_year + n;
    if ( (year % 400) == 0 )
       return true;
    else if ( (year % 100) == 0 )
       return false;
    else if ( (year % 4) == 0 )
       return true;
    else
       return false;
}
