#include "calendar.h"
#include <iostream>
#include <stdexcept>      // std::out_of_range

#define DEBUG 0

// Skip Lab2:: everywhere.
using namespace lab2;
using namespace std;

/**
  * This is the default constructor.
  */
template<class T>
Calendar<T>::Calendar(void) {
    if (DEBUG) {
        std::cout << std::endl << "lab2::Calendar(void) - default konstruktorn" << std::endl;
    }
    m_current_date = T();
}

/**
  * Copy constructor
  */
template<class T>
template<class S>
Calendar<T>::Calendar(Calendar<S> cs) {
    *this = cs; 
}

/**
  * Deconstructor
  */
template<class T>
Calendar<T>::~Calendar(void) {
    if (DEBUG) {
        std::cout << std::endl << "lab2::~Calendar(void) - default dekonstruktorn" << std::endl;
    } 
}

/** 
  * Assignment operator.
  */
template<class T>
template<class S>
Calendar<T>& Calendar<T>::operator=(const Calendar<S>& rhs) {
    this->m_current_date = rhs.m_current_date;

    // Clear my list of events
    this->m_events.clear();

    // Loop through rhs events, and add & conver date to mine
    Event ep = Event();
    for(int i = 0; i < rhs.m_events.size(); i++) {
        ep.description = rhs.m_events[i].description;
        ep.m_date = rhs.m_events[i].m_date;
        this->m_events.push_back(ep);
    }

    return *this;
}

template<class T>
Calendar<T>& Calendar<T>::operator=(const Calendar<T>& rhs) {
    // Clear my list of events
    this->m_events.clear();

    this->m_current_date = rhs.m_current_date;
    this->m_events = rhs.m_events;

    return *this;
}

/**
 * Set the current date of the calendar
 */
template<class T>
bool Calendar<T>::set_date(int year, int month, int day) {
    try {
        m_current_date = T(year, month, day);
    } catch (const std::out_of_range& oor) {
        return false;
    }

    return true;
}

/**
 * Add an event to the calendar
 * Note: Default values on year.
 */
template<class T>
bool Calendar<T>::add_event(std::string description) {
    int day  = m_current_date.day();
    int month = m_current_date.month();
    int year = m_current_date.year();
    return add_event(description, day, month, year);
}
/**
 * Add an event to the calendar
 * Note: Default values on month, year.
 */
template<class T>
bool Calendar<T>::add_event(std::string description, int day) {
    int month = m_current_date.month();
    int year = m_current_date.year();
    return add_event(description, day, month, year);
}

/**
 * Add an event to the calendar
 * Note: Default values on year.
 */
template<class T>
bool Calendar<T>::add_event(std::string description, int day, int month) {
    int year = m_current_date.year();
    return add_event(description, day, month, year);
}

/**
 * Add an event to the calendar
 * Note: Default values on year/month/day = -1
 */
template<class T>
bool Calendar<T>::add_event(std::string description, int day, int month, int year) {
    // Create a date object to get todays date 
    T today = T();

    // Check if the event is already in the vector
    for(auto e : m_events) {
        if(e.description.compare(description) == 0 && 
            e.m_date.day() == day && 
            e.m_date.month() == month && 
            e.m_date.year() == year) {
                return false;
        }
    }
    // Create the event and add it to our event list.
    Event e;
    e.description = description;
    try {
        e.m_date = T(year, month, day);
    } catch (const std::out_of_range& oor) {
        return false;
    }

    for(int i = 0; i < m_events.size(); i++) {
        if(e.m_date < m_events[i].m_date) {
            m_events.insert(m_events.begin() + i, e);
            return true;
        }    
    }
    m_events.push_back(e);
    return true;
}

/**
 * Removes an event from the calendar
 * Note: Default values on year.
 */
template<class T>
bool Calendar<T>::remove_event(std::string description, int day, int month) {
    int year = m_current_date.year();
    return remove_event(description, day, month, year);
}

/**
 * Removes an event from the calendar
 * Note: Default values on month and year.
 */
template<class T>
bool Calendar<T>::remove_event(std::string description, int day) {
    int year = m_current_date.year();
    int month= m_current_date.month();
    return remove_event(description, day, month, year);
}

/**
 * Removes an event from the calendar
 * Note: Default values on day, month and year.
 */
template<class T>
bool Calendar<T>::remove_event(std::string description) {
    int day  = m_current_date.day();
    int month = m_current_date.month();
    int year = m_current_date.year();
    return remove_event(description, day, month, year);
}
/**
 * Removes an event from the calendar
 * Note: Default values on year/month/day = -1
 */
template<class T>
bool Calendar<T>::remove_event(std::string description, int day, int month, int year) {
    // Set default to todays value if not provided (default value is -1)

    // Check if the event is already in the vector
    for(int i = 0; i < m_events.size(); i++) {
        auto e = m_events[i];
        if(e.description.compare(description) == 0 && 
            e.m_date.day() == day && 
            e.m_date.month() == month && 
            e.m_date.year() == year) {
                m_events.erase(m_events.begin() + i);
                return true;
        }
    }
    return false;
}

/**
 * Prints the date on the format YYYY-MM-DD
 * returns the ostream for chaining
 */
template<class S>
std::ostream& operator<<( std::ostream& os, const Calendar<S>& calendar ) {
    for(auto e : calendar.m_events) {
        if(e.m_date > calendar.m_current_date)
            os << e.m_date << " : " << e.description << endl;
    }
    return os;
}
