GCC = g++
CFLAGS  = -g -Wall --std=c++0x
cxxtestpath = /info/cprog11/cxxtest/
testcode = test.h

testcode.cpp: $(testcode)
	python $(cxxtestpath)cxxtestgen.py --error-printer -o testcode.cpp $(testcode)

datetest: datetest.cpp datetest.cpp date.o gregorian.o julian.o kattistime.o
	$(GCC) $(CFLAGS) datetest.cpp date.o gregorian.o julian.o kattistime.o -o datetest

testprogram: testcode.cpp $(testcode) date.o gregorian.o julian.o kattistime.o calendar.h calendar.tpp
	g++ --std=c++0x -I $(cxxtestpath) -o testprogram testcode.cpp date.o gregorian.o julian.o kattistime.o calendar.h

test:
	make testcode.cpp
	make testprogram

date.o: date.cpp date.h
	$(GCC) -c $(CFLAGS) date.cpp
    
gregorian.o: gregorian.cpp gregorian.h date.o
	$(GCC) -c $(CFLAGS) gregorian.cpp

julian.o: julian.cpp julian.h date.o
	$(GCC) -c $(CFLAGS) julian.cpp

kattistime.o: kattistime.h kattistime.cpp
	$(GCC) -c $(CFLAGS) kattistime.cpp

clean:
	rcsclean
	rm -f *.o
	rm testcode.cpp
	rm testprogram
	rm datetest
	rm cprog09lab22a

cprog09lab22a: date.o gregorian.o julian.o kattistime.o calendar.h
	$(GCC) $(CFLAGS) cprog09lab22a.cpp date.o gregorian.o julian.o kattistime.o -o cprog09lab22a

cprog09lab22b: date.o gregorian.o julian.o kattistime.o calendar.h
	$(GCC) $(CFLAGS) cprog09lab22b.cpp date.o gregorian.o julian.o kattistime.o -o cprog09lab22b

cprog09lab22c: date.o gregorian.o julian.o kattistime.o calendar.h
	$(GCC) $(CFLAGS) cprog09lab22c.cpp date.o gregorian.o julian.o kattistime.o -o cprog09lab22c

cprog09lab23: date.o gregorian.o julian.o kattistime.o calendar.h calendar.tpp
	$(GCC) $(CFLAGS) cprog09lab23.cpp date.o gregorian.o julian.o kattistime.o calendar.h -o cprog09lab23
